import { useState, useEffect } from "react";
import { Link } from "react-router-dom";

import { Card, Button} from "react-bootstrap";

export default function ProductCard({productProp}){
	const {_id, productName, description, price, stocks} = productProp;

	return (
	<Card className="p-3 my-3">
			    <Card.Body>
			        <Card.Title>
			            {productName}
			        </Card.Title>
			        <Card.Subtitle>Description: </Card.Subtitle>
			        <Card.Text>
			            {description}
			        </Card.Text>
			        <Card.Subtitle>Price: </Card.Subtitle>
			        <Card.Text>
			        {price}
			        </Card.Text>
			        <Card.Text>
			       		 Stocks: {stocks}
			        </Card.Text>
			        <Button as={Link} to={`/product/${_id}`} variant="primary">Details</Button>
			        

			    </Card.Body>
			</Card>
			)
	}