// import { Link } from "react-router-dom";
// import {useState, useContext} from "react";
import UserContext from "../UserContext"
import { Link } from "react-router-dom";
import {useState, useContext} from "react";
import "../styles/navbar.css"

import {Navbar, Nav, Container, NavDropdown} from "react-bootstrap";



export default function AppNavbar(){
	const {user} = useContext(UserContext);



	return(
		<Navbar id="navbar" bg="light" expand="lg">
		      <Container>
		        <Navbar.Brand as={Link} to="/">E-commerce</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          <Nav className="ms-auto">
		            <Nav.Link as={Link} to="/" eventKey="/">Home</Nav.Link>
		            <Nav.Link as={Link} to="/product" eventKey="/product">Product</Nav.Link>
		            {	
	            	(user.id !== null)
	            	?	
	            		<>
	            		<Nav.Link as={Link} to="/userOrder" eventKey="/userOrder">Orders</Nav.Link>
	            		<Nav.Link as={Link} to="/logout" eventKey="/logout">Logout</Nav.Link>
	            		</>
	            	:
	            		<>
		            		<Nav.Link as={Link} to="/login" eventKey="/login">Login</Nav.Link>
		            		<Nav.Link as={Link} to="/register" eventKey="/register">Register</Nav.Link>
	            		</>
	            	}
		          </Nav>
		        </Navbar.Collapse>
		      </Container>
		    </Navbar>
		  );
}
