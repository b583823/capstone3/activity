import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import "../styles/products.css";
import { Card, Button, Col} from "react-bootstrap";

import "../styles/card.css"


 export default function ProductCard({productProp}){
 	const {_id, productName, description, price, stocks, thumbnail} = productProp;


 return (
  <Col xs={12} md={3} className="mb-5">
    <Card className="m-1"  style={{ width: '18rem',height:'500px' }}>
      <Card.Img variant="top" src={thumbnail} />
      <Card.Body>
        <Card.Title>{productName}</Card.Title>
        <Card.Text>
          {description}
        </Card.Text>
        <Card.Text>
         PHP {price} - Stocks:{stocks}
        </Card.Text>
     <div className="mt-auto">
       <Button  as={Link} to={`/product/${_id}`} size="sm" variant="primary">Details</Button>
     </div>
      </Card.Body >
    </Card>
    </Col>
 
  );


}