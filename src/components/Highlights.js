import {Row, Col, Card, Carousel} from "react-bootstrap";
import "../styles/highlights.css"

export default function Highlights(){
	return(
			<Row className="mt-3 mb-3">
			    <Col xs={12} md={4}>
			        <Card className="cardHighlight p-3">
			            <Card.Body>
			            <Card.Title>
			                    <h2 className="text-center">Best Seller</h2>
			                </Card.Title>
			                
			                <Carousel id="carousel" variant="dark">
			                      <Carousel.Item interval={2000}>
			                        <img
			                          className="d-block w-100"
			                          src="https://www.relumins.com/assets/images/SKS-KOJIESOAPX3-100-FBA.jpg"
			                          alt="First slide"
			                        />
			                        
			                      </Carousel.Item>
			                      <Carousel.Item interval={2000}>
			                        <img
			                          className="d-block w-100"
			                          src="https://cf.shopee.ph/file/0b7f42432ccf865aa1c46a792d7c047d"
			                          alt="Second slide"
			                        />
			                        
			                      </Carousel.Item>
			                      <Carousel.Item interval={2000}>
			                        <img
			                          className="d-block w-100"
			                          src="https://3.bp.blogspot.com/-nCZAbw_9S4Y/VzWNP_rDe8I/AAAAAAABIx0/KzSFWQy19KEha9wQNUJVhm5SfkM9sZEWACKgB/s640/Kojie%2BSan%2Bskin%2Blightening%2Bsoap%2Breview-soap.JPG"
			                          alt="Third slide"
			                        />
			                      </Carousel.Item>
			                    </Carousel>
			            </Card.Body>
			        </Card>
			    </Col>
			    <Col xs={12} md={4}>
			        <Card className="cardHighlight p-3">
			            <Card.Body>
			                <Card.Title>
			                    <h2 className="text-center">Featured Brands</h2>
			                </Card.Title>
			                
			                <Carousel id="carousel" variant="dark">
			                      <Carousel.Item interval={2000}>
			                        <img
			                          className="d-block w-100"
			                          src="https://cosmetics.infojashore.com/assets/uploads/Printable-Tresemme-Co.jpg"
			                          alt="First slide"
			                        />
			                        
			                      </Carousel.Item>
			                      <Carousel.Item interval={2000}>
			                        <img
			                        className="d-block w-100"
			                          src="https://i.dailymail.co.uk/1s/2022/02/22/15/54508967-0-image-a-12_1645544074496.jpg"
			                          alt="secon= slide"
			                        />
			                        
			                      </Carousel.Item>
			                      <Carousel.Item interval={2000}>
			                        <img
			                          className="d-block w-100"
			                          src="https://elleinadsays.files.wordpress.com/2013/08/acnesolutions_products_0.png"
			                          alt="Third slide"
			                        />
			                      </Carousel.Item>
			                    </Carousel>
			            </Card.Body>
			        </Card>
			    </Col>
			    <Col xs={12} md={4}>
			        <Card className="cardHighlight p-3">
			            <Card.Body>
			                 <Card.Title>
			                    <h2 className="text-center">Products On Sale</h2>
			                </Card.Title>
			                
			                <Carousel id="carousel" variant="dark">
			                      <Carousel.Item interval={2000}>
			                        <img
			                          className="d-block w-100"
			                          src="https://cloudinary.images-iherb.com/image/upload/f_auto,q_auto:eco/images/adc/adc00801/l/17.jpg"
			                          alt="First slide"
			                        />
			                        
			                      </Carousel.Item>
			                      <Carousel.Item interval={2000}>
			                        <img
			                          className="d-block w-100"
			                          src="https://i.ebayimg.com/images/g/E4sAAOSwFalcdEEG/s-l500.jpg"
			                          alt="Second slide"
			                        />
			                        
			                      </Carousel.Item>
			                      <Carousel.Item interval={2000}>
			                        <img
			                          className="d-block w-100"
			                          src="https://www.emeds.pk//upload/pro-imges/image-12/0223.jpg"
			                          alt="Third slide"
			                        />
			                      </Carousel.Item>
			                    </Carousel>
			            </Card.Body>
			        </Card>
			    </Col>
			</Row>
		)
}