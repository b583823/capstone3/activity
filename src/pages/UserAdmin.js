import { useContext, useState, useEffect } from "react";
import {Table, Button} from "react-bootstrap";
import UserContext from "../UserContext";
import {Navigate, useNavigate} from "react-router-dom";
import Swal from "sweetalert2"

export default function UserAdmin(){
	const {user} = useContext(UserContext);
	console.log(user)
	const navigate = useNavigate();
	const [allUsers, setAllUsers] = useState([]);
	const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/users/getAllUsers`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllUsers(data.map(users => {
				return(
					<tr key={users._id}>
						<td>{users._id}</td>
						<td>{users.email}</td>
						<td>{users.firstName}</td>
						<td>{users.lastName}</td>
						<td>{users.isAdmin ? "Admin" : " Non-Admin"}</td>
						<td>
						{
							(users.isAdmin)
							?
								<Button variant="danger" size="sm" onClick ={() => setNonAdmin(users._id, users.email)}>Set Non-Admin</Button>
							:
								<>
									<Button variant="danger" size="sm" onClick ={() => setAdmin(users._id, users.email)}>Set Admin</Button>
								</>
						}
						</td>
					</tr>
				)
			}))

		})
	}


	
	const setAdmin=(userId, userEmail) =>{
		fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/setAsAdmin`,{
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data);
			if(data){
				Swal.fire({
					title: "Update Successfull!",
					icon: "success",
					text: `${userEmail} is now Admin.`
				})
			fetchData();
			}
			else{
				Swal.fire({
					title: "Update Unsuccessfull!",
					icon: "error",
					text: `Something went wrong. Please try again later.`
				})
			}
		})
	}
	const setNonAdmin=(userId, userEmail) =>{
		fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/setNonAdmin`,{
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data);
			if(data){
				Swal.fire({
					title: "Admin Status Updated!",
					icon: "success",
					text: `${userEmail} is now Non-admin.`
				})
			fetchData();
			}
			else{
				Swal.fire({
					title: "Updating Unsuccessfull!",
					icon: "error",
					text: `Something went wrong. Please try again later.`
				})
			}
		})
	}
	
	// 

	// To fetch all courses in the first render of the page.
	useEffect(()=>{
		// invoke fetchData() to get all courses.
		fetchData();
	}, [])


	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1>User List</h1>
				
			</div>
			<Table striped bordered hover>
		     <thead className="text-center">
		       <tr>
		         <th>User ID</th>
		         <th>User Email</th>
		         <th>First Name</th>
		         <th>Last Name</th>
		         <th>Status</th>
		         <th>Action</th>
		       </tr>
		     </thead>
		     <tbody>
		       { allUsers }
		     </tbody>
		   </Table>

		</>
		:
		<Navigate to="/product" />


		)
}