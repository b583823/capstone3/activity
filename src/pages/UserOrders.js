import { useContext, useState, useEffect } from "react";
import {Accordion, Button} from "react-bootstrap";
import UserContext from "../UserContext";
import {Navigate} from "react-router-dom";


export default function UserOrders(){
	const {user} = useContext(UserContext);
	const [userOrder, setUserOrder] = useState([]);
	let i = 1;

	const fetchData = () =>{
			fetch(`${process.env.REACT_APP_API_URL}/users/orders`,{
				headers:{
					"Authorization": `Bearer ${localStorage.getItem("token")}`
				}
			})
			.then(res => res.json())
			.then(data => {
				setUserOrder(data.orders.map((order,index) => {
					
					return (
						<Accordion key={order._id}>
						      <Accordion.Item eventKey={order._id}>
						        <Accordion.Header>Order #{index+1}: {order.purchasedOn}</Accordion.Header>
						        <Accordion.Body>
						         	<p>Product Name: {order.productName}</p>
						         	<p>Quantity: {order.quantity}</p>
						         	<h6>Total Amount: {order.totalAmount}</h6>
						         
						        </Accordion.Body>
						      </Accordion.Item>
						</Accordion>


						)

				}))
			})
			}
	useEffect(()=>{
			// invoke fetchData() to get all courses.
			fetchData();
		}, [])

	return (
		(user.id!==null)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1>Order History</h1>
				
			</div>

			<div>
				{userOrder}

			</div>
			

		</>
		:
		<Navigate to="/" />




		)
}