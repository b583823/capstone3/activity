import { useContext, useState, useEffect } from "react";
import {Table, Button} from "react-bootstrap";
import {Navigate, useNavigate} from "react-router-dom";
import UserContext from "../UserContext";

import UpdateModal from "./UpdateModal";
import AddProduct from "./AddProduct";

import Swal from "sweetalert2"

export default function AdminDashboard(){

	const {user} = useContext(UserContext);
	const navigate = useNavigate();
	const [allProducts, setAllProducts] = useState([]);
	const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/allProduct`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllProducts(data.map(product => {
				return(
					<tr key={product._id}>
						<td>{product._id}</td>
						<td>{product.productName}</td>
						<td>{product.description}</td>
						<td>{product.price}</td>
						<td>{product.stocks}</td>
						<td>{product.isActive ? "Active" : "Inactive"}</td>
						<td>
						{
							(product.isActive)
							?
								<Button variant="danger" size="sm" onClick ={() => archive(product._id, product.productName)}>Archive</Button>
							:
								<>
									<Button variant="success" size="sm" onClick ={() => unarchive(product._id, product.productName)}>Unarchive</Button>
									<UpdateModal productProp={product}/>
								</>
						}
						</td>
					</tr>
				)
			}))

		})
	}


	
	const archive=(productId, productName) =>{
		console.log(productId);
		console.log(productName);
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`,{
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data);
			if(data){
				Swal.fire({
					title: "Archive Successfull!",
					icon: "success",
					text: `${productName} is now inactive.`
				})
			fetchData();
			}
			else{
				Swal.fire({
					title: "Archive Unsuccessfull!",
					icon: "error",
					text: `Something went wrong. Please try again later.`
				})
			}
		})
	}
	const unarchive=(productId, productName) =>{
		console.log(productId);
		console.log(productName);
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/unarchive`,{
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data);
			if(data){
				Swal.fire({
					title: "Unarchive Successfull!",
					icon: "success",
					text: `${productName} is now active.`
				})
			fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessfull!",
					icon: "error",
					text: `Something went wrong. Please try again later.`
				})
			}
		})
	}
	
	// 

	
	useEffect(()=>{
		
		fetchData();
	}, [])


	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1>Admin Dashboard</h1>
				<AddProduct />
				<Button variant="success" size="lg" className="mx-2 my-1" onClick ={() => navigate("/orders")}>Show Order History</Button>
			</div>
			<Table striped bordered hover>
		     <thead className="text-center">
		       <tr>
		         <th>Product ID</th>
		         <th>Product Name</th>
		         <th>Description</th>
		         <th>Price</th>
		         <th>Stocks</th>
		         <th>Status</th>
		         <th>Action</th>
		       </tr>
		     </thead>
		     <tbody>
		       { allProducts }
		     </tbody>
		   </Table>

		</>
		:
		<Navigate to="/product" />
	)
}