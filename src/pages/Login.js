import {useState, useEffect, useContext} from "react";
import UserContext from "../UserContext";


import { Navigate } from "react-router-dom";
import { Form, Button } from "react-bootstrap";
import Swal from "sweetalert2";




export default function Login(){
	const {user, setUser} = useContext(UserContext);
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState(false);

	function login(e){
		e.preventDefault();
		console.log(e)
		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			console.log(data.access)
			if(typeof data.access!=="undefined"){
			localStorage.setItem("token", data.access);
			retrieveUserDetails(data.access);
			Swal.fire({
				title: "Login Successful",
				icon: "success",
				text: "Welcome"
			})
			}
			else{
				Swal.fire({
				title: "Authentication Failed",
				icon: "error",
				text: "Check your login details and try again."
			})

			}
		});
		setEmail("");
		setPassword("");

	}

	const retrieveUserDetails = (token) =>{
		fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data);

			// This will be set to the user state
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(() =>{
		// Validation to enable the submit button when all fields are populated.
		if(email !== "" && password !== ""){
			setIsActive(true)
		}
		else{
			setIsActive(false)
		}
	}, [email, password])

	return(
		(user.id !==null)
		?
			<Navigate to="/product" />
		:
		<>
		<h1 className="my-5 text-center">Login</h1>
		<Form onSubmit ={(e) => login(e)}>
		      <Form.Group className="mb-3" controlId="userEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e=> setEmail(e.target.value)} />
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="password">
		        <Form.Label>Password</Form.Label>
		        <Form.Control type="password" placeholder="Password" value={password} onChange={e=> setPassword(e.target.value)} />
		      </Form.Group>
		  		{
		  			isActive
		  			?
		  				<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
		  			:
		  			<Button variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
		  		}
		</Form>
		</>
			
)}
