import Banner from "../components/Banner";
import Highlights from "../components/Highlights";

export default function Home(){
	const data = {
		title: "Beauty Options",
		content:"Health is Wealth but Beauty is Divine",
		destination: "/product",
		label: "Shop Now"
	}
	return(
		<>
		 	<Banner data={data} />
        	<Highlights />
        	
		</>
		)
}