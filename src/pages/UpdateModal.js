import {useState, useEffect} from "react";
import {Modal, Button, Form} from "react-bootstrap";
import Swal from "sweetalert2";
import {Navigate, useNavigate} from "react-router-dom";

export default function UpdateProduct({productProp}) {
  const [show, setShow] = useState(false);
  const {_id, productName, description, price, stocks, thumbnail, pic1,pic2,pic3} = productProp
  const navigate = useNavigate();
  const [isActive, setIsActive] = useState(false);
  // const [productId, setProductId] = useState("");

  const [name, setName] = useState(productName);
  const [prodDesc, setProdDesc] = useState(description);
  const [prodPrice, setProdPrice] = useState(price);
  const [prodStocks, setProdStocks] = useState(stocks);
  const [pic, setPic] = useState(thumbnail);
  const [hpic1, setHpic1] = useState(pic1);
  const [hpic2, setHpic2] = useState(pic2);
  const [hpic3, setHpic3] = useState(pic3);


  const handleClose = () => setShow(false);
  const handleShow = () =>  setShow(true);
    
  
    // fetch(`${process.env.REACT_APP_API_URL}/products/${_id}`)
    // .then(res=>res.json())
    // .then(data=>{
    //   console.log(data);

    // }
  
function update(e) {

  e.preventDefault();
  fetch(`${process.env.REACT_APP_API_URL}/products/${_id}`, {
    method: "PUT",
    headers:{
      "Authorization": `Bearer ${localStorage.getItem("token")}`,
      "Content-Type":"application/json"

    },
    body: JSON.stringify({
        productName: name,
        description: prodDesc,
        price: prodPrice,
        stocks: prodStocks,
        thumbnail: pic,
        pic1: hpic1,
        pic2: hpic2,
        pic3: hpic3
    })
  })
  .then(res => res.json())
  .then(data => {
    if(data){
      Swal.fire({
        title: "Update successful",
        icon: "success",
        text: `You have successfully updated ${name}`
      })

    }

    else{
      Swal.fire({
        title: "Something went wrong",
        icon: "error",
        text: "Please try again."
      })
    }
    navigate("/product");

  })
}

useEffect(()=>{
    if((name !== "" && prodDesc !== "" && prodPrice !== "" && prodStocks !== ""&& pic!==""&& hpic1!==""&& hpic2!==""&& hpic3!=="")){
      setIsActive(true);
    }
    else{
      setIsActive(false);
    }
  },[name, prodDesc, prodPrice, prodStocks,pic, hpic1,hpic2,hpic3])


  return (
    <>
      <Button className="align-center mx-2"size="sm" variant="primary" onClick={handleShow}>
        Edit
      </Button>

      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
        centered
        
      >
        <Modal.Header>
          <Modal.Title>Update: {productName}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form >
                  <Form.Group className="mb-3" controlId="userEmail">
                    <Form.Label>Product Name:</Form.Label>
                    <Form.Control type="string" value={name} onChange={e=>setName(e.target.value)}/>
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="password">
                    <Form.Label>Product Description: </Form.Label>
                    <Form.Control ype="string" value={prodDesc} onChange={e=>setProdDesc(e.target.value)} />
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="password">
                    <Form.Label>Price: </Form.Label>
                    <Form.Control type="number" value={prodPrice} onChange={e=>setProdPrice(e.target.value)} />
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="password">
                    <Form.Label>Stocks: </Form.Label>
                    <Form.Control type="number" value={prodStocks} onChange={e=>setProdStocks(e.target.value)} />
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="password">
                    <Form.Label>Thumbnail: </Form.Label>
                    <Form.Control type="string" value={pic} onChange={e=>setPic(e.target.value)} />
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="password">
                    <Form.Label>Picture 1: </Form.Label>
                    <Form.Control type="string" value={hpic1} onChange={e=>setHpic1(e.target.value)} />
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="password">
                    <Form.Label>Picture 2: </Form.Label>
                    <Form.Control type="string" value={hpic2} onChange={e=>setHpic2(e.target.value)} />
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="password">
                    <Form.Label>Picture 3: </Form.Label>
                    <Form.Control type="string" value={hpic3} onChange={e=>setHpic3(e.target.value)} />
                  </Form.Group>
            </Form>
          
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Discard
          </Button>
          {
          (isActive)
          ?
          <Button variant="primary"  onClick ={(e) => update(e)}>Save</Button>
          :
          <Button variant="primary"  onClick ={(e) => update(e)} disabled>Save</Button>
           }
        </Modal.Footer>
      </Modal>
    </>
  );
}
// onSubmit ={(e) => login(e)