import {useState, useEffect, useContext} from "react";
import {Link, useParams, useNavigate} from "react-router-dom";
import {Container, Card, Button, Row, Col, Carousel} from "react-bootstrap";
import UserContext from "../UserContext";
import Swal from "sweetalert2";
import "../styles/productview.css"

export default function ProductView(){
	const { user } = useContext(UserContext);

	const navigate = useNavigate()
	const { productId } = useParams();
	
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [hpic1, setHpic1] = useState("");
	const [hpic2, setHpic2] = useState("");
	const [hpic3, setHpic3] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(0);
	const [total, setTotal] = useState(0);
	const [isActive, setIsActive] = useState(0);

	const product = (productId) =>{
		fetch(`${process.env.REACT_APP_API_URL}/users/createOrder`,{
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({

				productId: productId,
				quantity: quantity
			})
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data);
			if(data === true){
			Swal.fire({
				title: "Order Success",
				icon: "success",
				text: "You have successfully ordered this product."
			})
			navigate("/product");
			}
			else{
				Swal.fire({
				title: "Something went wrong",
				icon: "error",
				text: "Please try again."
			})
			}

		})
	}

	useEffect(()=>{

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res=> res.json())
		.then(data=>{
			console.log(data);
			setName(data.productName);
			setDescription(data.description);
			setPrice(data.price)
			setHpic1(data.pic1)
			setHpic2(data.pic2)
			setHpic3(data.pic3)
		})

	},[productId])

	useEffect(()=>{
		setTotal(quantity*price)
	},[quantity])

	useEffect(()=>{
		console.log(quantity)
	    if(quantity>0){
	      setIsActive(true);
	    }
	    else{
	      setIsActive(false);
	    }
	  },[quantity])

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Carousel id="mycarousel" variant="dark">
					      <Carousel.Item interval={1000} >
					      
					        <img
					          className="d-block w-100 img-fluid"
					          src={hpic1}
					          alt="First slide"
					        />
					      
					      </Carousel.Item>
					      <Carousel.Item interval={1000}>
					      
					        <img
					          className="d-block w-100 img-fluid"
					          src={hpic2}
					          alt="Second slide"
					        />
					       
					      </Carousel.Item>
					      <Carousel.Item interval={1000}>
					      
					        <img
					          className="d-block w-100 img-fluid"
					          src={hpic3}
					          alt="Third slide"
					        />
					        
					      </Carousel.Item>
					</Carousel>
					<Card>
						<Card.Body className="text-center">
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>PhP {price}</Card.Text>
							<div className = "d-grid gap-2">
								{
									(user.id !== null)
									?
									<Container>	
										<Row >
											<Col className="col-3">
												<p>Quantity:</p>
												 <input className="w-50" type="number" value={quantity} onChange={e=>{setQuantity(e.target.value); console.log(quantity); console.log(isActive)}} />
											</Col>
											<Col className="col-3">
												<p>Total:</p>
												<p>{total}</p>
											</Col>
											{
											(isActive)
											?
											<Col className="col-6 my-auto">
												
												<Button className="px-4"variant="primary" size="lg" onClick={()=> product(productId)}>Buy</Button>
											</Col>
											:
											<Col className="col-6 my-auto">
												
												<Button className="px-4"variant="primary" size="lg" onClick={()=> product(productId)} disabled >Buy</Button>
											</Col>
											}
										</Row>
									</Container>
									:
									<Button as={Link} to="/login" variant="primary"  size="lg">Login to buy this product</Button>
								}
							</div>
						</Card.Body>		
					</Card>
				</Col> 	
			</Row>
		</Container>
		)
}