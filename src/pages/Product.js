import {useEffect, useState, useContext} from "react";
import {Navigate, useNavigate} from "react-router-dom";
import Card from "../components/Card";
import UserContext from "../UserContext";
import {Button, Row} from "react-bootstrap";

export default function Product(){
	const [product, setProduct] = useState([])
	const navigate = useNavigate();
	const {user} = useContext(UserContext);
	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/products/activeProduct`)
		.then(res=>res.json())
		.then(data=>{
			console.log(data)
			setProduct(data.map(product=>{
				return (

					<Card key={product._id} productProp={product} />
					)
			}))
		})

	},[])
	return(
		(user.isAdmin)
		?
		<Navigate to="/admin" />
		:
			<>
				<h1 className="text-center my-3">Product</h1>
				<Row>
				{product}
				</Row>
			</>
		)

}